const express = require("express");
const { get } = require("http");
const app = express();
const PORT = 3000;
//Build small REST API with Express
console.log("Server side program starting")

// baseurl: http://localhost:3000
// Endpoint: http://localhost:3000/
app.get("/", (req, res)=> {
    res.send("Hello World");
});

app.listen(PORT, () => console.log(
    'Server listening http://localhost:${PORT}'
));